import os
import discord
from dotenv import load_dotenv
import requests

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = discord.Client()


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if 'mangadex.org' in message.content:
        urls = [
            s for _, s in enumerate(
                message.content.split()) if 'mangadex.org' in s]
        split = [i.split('/') for i in urls]
        stripped = [i[i.index('mangadex.org') + 1:] for i in split]
        for i in stripped:
            result = process_data(*i)
            await message.channel.send(embed=result)


def process_data(endpoint, slug, page=None):
    chapter_info = ''
    if endpoint == 'chapter':
        chapter = requests.get(f'https://api.mangadex.org/chapter/{slug}')
        if chapter.status_code == 200:
            attrs = chapter.json().get('data', {}).get('attributes', {})
            chapter_number = attrs.get('chapter', '').replace('.', '-')
            page_num = page if(page) else 1
            chapter_info = f'{chapter_number}/{page_num}'
            rels = chapter.json().get('relationships', {})
            for r in rels:
                if r.get('type', '') == 'manga':
                    chapter_slug = slug
                    slug = r.get('id', slug)
        else:
            return embed(
                error='Unable to find manga asscoiated with chapter id:{}'.format(slug))

    manga_request = requests.get(
        'https://api.mangadex.org/manga/{}?includes[]=author&includes[]=artis&includes[]=cover_art'.format(slug))

    if manga_request.status_code == 200:
        json = manga_request.json()
        attrs = json.get('data', {}).get('attributes', {})
        relationships = json.get('relationships', {})
        # Because the language of the title is inconsistent, just grab the
        # first one it finds
        title = list(attrs.get('title', {'any': None}).values())[0]
        desc = attrs.get('description', {}).get('en', None)
        author = None
        artist = None
        cover = None
        for rel in relationships:
            if rel.get('type', '') == 'author':
                author = rel.get('attributes', {}).get('name', None)
                if author is None:
                    author_id = rel.get('id', '')
                    author = expand_author(author_id)
            if rel.get('type', '') == 'artist':
                artist = rel.get('attributes', {}).get('name', None)
                if artist is None:
                    artist_id = rel.get('id', '')
                    artist = expand_author(artist_id)
            if rel.get('type', '') == 'cover_art':
                cover = rel.get('attributes', {}).get('fileName', None)
        return embed(title, desc, author, artist, cover, slug, chapter_info)
    else:
        return embed(error='Unable to load manga with id={}'.format(slug))


def expand_author(author_id):
    # Sometimes authors/artists don't auto expand
    author_req = requests.get(f'https://api.mangadex.org/author/{author_id}')
    if author_req.status_code == 200:
        return author_req.json().get(
            'data',
            {}).get(
            'attributes',
            {}).get(
            'name',
            None)


def embed(
        title=None,
        desc=None,
        author=None,
        artist=None,
        cover=None,
        slug=None,
        chapter_info='',
        error=None):
    if error:
        return discord.Embed(
            title='Unable to load information from the link',
            description=error,
            color=discord.Color.red())

    manga_embed = discord.Embed(
        title=title,
        description=desc,
        url=f'https://cubari.moe/read/mangadex/{slug}/{chapter_info}',
        color=discord.Color.green())
    manga_embed.set_thumbnail(
        url=f'https://uploads.mangadex.org/covers/{slug}/{cover}')
    if(author):
        manga_embed.add_field(name='Author', value=author, inline=True)
    if(artist):
        manga_embed.add_field(name='Artist', value=artist, inline=True)
    return manga_embed


client.run(TOKEN)
